# Pathfinder 2
*This is an app to manage everything that is going on in a tabletop RPG on your
computer.*

As of now, this registers and shows basic information about players and it can
be used to make dice rolls. To roll dice, this
app uses the `randomgen` python package because I am against the Mersenne
Twister RNG. Rest assured that the RNG is reseeded in a NON-DETERMINISTIC way
at each new instanciation. This means that the first 4 dice rolls are completely
random. After that, the program produces a pseudo-random sequence, but,
since the seed is really random, this is good enough to simulate dice rolls.

For more info on what the packages do, just look into the `__init__.py` in each
of them as they contain a description.

# Installation and usage

Just download and hope that your [Python 3](https://www.python.org/) interpreter
will execute `main.py` correctly. You might need to install
[PyQt5](https://www.riverbankcomputing.com/software/pyqt/intro) and
[randomgen](https://github.com/bashtage/randomgen). I am building and testing
the program on my personnal laptop running [Solus](https://www.solus-project.com)
always kept up to date. I also sometimes test this stuff in a Windows VM.

## todo (From here these are my personnal notes in french)
- Implémenter la feuille de perso au complet
  - Logique pour les skills dans la création de personnage
  - Implémenter les skills
  - Implémenter les backgrounds
  - Ajouter le HP et la CA à la feuille d'attributs
  - Implémenter les gestion de feuilles de personnages incomplètes
  - Implémenter un système de level up
  - Implémenter les feats
  - Implémenter un système d'inventaire
  - Implémenter une option séance de jeu pour avoir des scores/bonus temporaire,
    du hp etc.
  - Vérifier que tous les champs d'une feuille de perso sont implémentés.
- Ajouter une gestion logique et correcte des erreurs dans l'UI
- Documenter les classes et styliser le code à peu près correctement
- Faire un bel UI aux dés

## Logique
Cette section contient une liste de ce qui doit être implémenté en terme de
logique et de la réflexion sur ce qui doit être fait techniquement.
- Les joueurs
  - Normalement les joueurs devraient être stockés dans une base de donnée pour
  être instanciés à chaque fois que le jeu se lance.
  - Il faut que les joueurs soient implémentés en tant que classe avec tous les
  détails de la feuille de personnage
- Les feats
- Le système de combat
- La caisse
- L'inventaire
- Les sorts

## Interface
Cette section contient une liste de ce qui doit être implémenté en terme de
l'interface et de la réflexion sur ce qui doit être fait techniquement.
- Une interface qui se lance avec un programme principal qui permet de modifier
la fiche des joueurs
