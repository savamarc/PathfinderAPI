"""
This package is the GUI, it reimplements various objects from PyQt5 to make
the not so beautiful windows of this program.
This package is also where most of the user interaction is programmed: for
instance, one could expect to find the character creation in the Player package,
but it is in this package because it is an interractive feature.

Everything in this package should make sure that it uses the good types when
interracting with the back end because the functions in it make no checks
whatsoever.
"""
