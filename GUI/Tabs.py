"""
This file contains the layouts that are used as tabs in the main GUI.
"""

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QGridLayout, QLabel, QComboBox,
        QPushButton, QDialog, QVBoxLayout, QHBoxLayout, QListWidget, QScrollArea)

from GUI.CharSheetRe import *
from Player.Player import *
from dice import *

class Dice(QWidget):

    def __init__(self):
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This function sets the properties of the die throwing tab
        """
        self.setWindowTitle('Slice and Dice')

        Layout = QGridLayout()
        Layout.setSpacing(10)

        # Fields in a description
        column1 = QLabel('Number of dice')
        column2 = QLabel('Type of dice')
        Layout.addWidget(column1, 1, 0, Qt.AlignHCenter)
        Layout.addWidget(column2, 1, 1, Qt.AlignHCenter)
        # d4
        self.d4 = QLabel('d4 =')
        self.nd4 = QComboBox()
        self.d4res = QPushButton('Throw!')
        self.d4res.clicked.connect(self.throwd4)
        for i in range(1, 16):
            self.nd4.addItem(str(i))
        Layout.addWidget(self.nd4, 2, 0)
        Layout.addWidget(self.d4, 2, 1)
        Layout.addWidget(self.d4res, 2, 2)
        # d6
        self.d6 = QLabel('d6 =')
        self.nd6 = QComboBox()
        self.d6res = QPushButton('Throw!')
        self.d6res.clicked.connect(self.throwd6)
        for i in range(1, 16):
            self.nd6.addItem(str(i))
        Layout.addWidget(self.nd6, 3, 0)
        Layout.addWidget(self.d6, 3, 1)
        Layout.addWidget(self.d6res, 3, 2)
        # d8
        self.d8 = QLabel('d8 =')
        self.nd8 = QComboBox()
        self.d8res = QPushButton('Throw!')
        self.d8res.clicked.connect(self.throwd8)
        for i in range(1, 16):
            self.nd8.addItem(str(i))
        Layout.addWidget(self.nd8, 4, 0)
        Layout.addWidget(self.d8, 4, 1)
        Layout.addWidget(self.d8res, 4, 2)
        # d10
        self.d10 = QLabel('d10 =')
        self.nd10 = QComboBox()
        self.d10res = QPushButton('Throw!')
        self.d10res.clicked.connect(self.throwd10)
        for i in range(1, 16):
            self.nd10.addItem(str(i))
        Layout.addWidget(self.nd10, 5, 0)
        Layout.addWidget(self.d10, 5, 1)
        Layout.addWidget(self.d10res, 5, 2)
        # d12
        self.d12 = QLabel('d12 =')
        self.nd12 = QComboBox()
        self.d12res = QPushButton('Throw!')
        self.d12res.clicked.connect(self.throwd12)
        for i in range(1, 16):
            self.nd12.addItem(str(i))
        Layout.addWidget(self.nd12, 6, 0)
        Layout.addWidget(self.d12, 6, 1)
        Layout.addWidget(self.d12res, 6, 2)
        # d20
        self.d20 = QLabel('d20 =')
        self.nd20 = QComboBox()
        self.d20res = QPushButton('Throw!')
        self.d20res.clicked.connect(self.throwd20)
        for i in range(1, 16):
            self.nd20.addItem(str(i))
        Layout.addWidget(self.nd20, 7, 0)
        Layout.addWidget(self.d20, 7, 1)
        Layout.addWidget(self.d20res, 7, 2)
        # d100
        self.d100 = QLabel('d100 =')
        self.nd100 = QComboBox()
        self.d100res = QPushButton('Throw!')
        self.d100res.clicked.connect(self.throwd100)
        for i in range(1, 16):
            self.nd100.addItem(str(i))
        Layout.addWidget(self.nd100, 8, 0)
        Layout.addWidget(self.d100, 8, 1)
        Layout.addWidget(self.d100res, 8, 2)


        self.setLayout(Layout)

    def throwd4(self):
        a = None
        n = int(self.nd4.currentText())
        a = d4(n)
        self.d4res.setText(str(sum(a)))

    def throwd6(self):
        a = None
        n = int(self.nd6.currentText())
        a = d6(n)
        self.d6res.setText(str(sum(a)))

    def throwd8(self):
        a = None
        n = int(self.nd8.currentText())
        a = d8(n)
        self.d8res.setText(str(sum(a)))

    def throwd10(self):
        a = None
        n = int(self.nd10.currentText())
        a = d10(n)
        self.d10res.setText(str(sum(a)))

    def throwd12(self):
        a = None
        n = int(self.nd12.currentText())
        a = d12(n)
        self.d12res.setText(str(sum(a)))

    def throwd20(self):
        a = None
        n = int(self.nd20.currentText())
        a = d20(n)
        self.d20res.setText(str(sum(a)))

    def throwd100(self):
        a = None
        n = int(self.nd100.currentText())
        a = d100(n)
        self.d100res.setText(str(sum(a)))

class CharView(QWidget):
    """
    This widget is the tab where the operations about the characters will be
    made. It offers a way of navigation through the existing characters, a way
    of modifiying these characters, a way to delete them, and a way to create
    new characters.
    """

    def __init__(self):
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This function sets the basic properties of the character viewing sheet.
        """
        self.setWindowTitle('Characters')
        Layout = QVBoxLayout()
        dataLayout = QHBoxLayout()
        self.charLayout = CharSheetUpdate()
        self.buttonsLayout = QHBoxLayout()
        Layout.addLayout(dataLayout)
        Layout.addLayout(self.buttonsLayout)

        # The buttons in the interface
        self.addbtn = QPushButton('Add a character')
        self.addbtn.clicked.connect(self.addChar)
        self.buttonsLayout.addWidget(self.addbtn)

        self.update = QStackedWidget()
        updatebtn = QPushButton('Update character')
        updatebtn.clicked.connect(self.updateChar)
        savebtn = QPushButton('Save')
        savebtn.clicked.connect(self.saveUpdate)
        self.update.addWidget(updatebtn)
        self.update.addWidget(savebtn)
        self.update.setSizePolicy(QSizePolicy(QSizePolicy.Fixed,
            QSizePolicy.Fixed, QSizePolicy.PushButton))
        self.buttonsLayout.addWidget(self.update)

        self.refreshbtn = QPushButton('Refresh')
        self.refreshbtn.clicked.connect(self.updateList)
        self.buttonsLayout.addWidget(self.refreshbtn)

        self.label = QLabel('')
        self.buttonsLayout.addWidget(self.label)

        self.buttonsLayout.addStretch(0)
        
        deletebtn = QPushButton('Delete character')
        deletebtn.clicked.connect(self.deleteChar)
        self.buttonsLayout.addWidget(deletebtn)

        # Setting dataLayout
        self.charList = QListWidget()
        self.charList.setSortingEnabled(True)
        self.updateList()
        self.charList.currentItemChanged.connect(self.changePlayer)
        dataLayout.addWidget(self.charList)

        #dataLayout.addStretch()
        widget1 = QWidget()
        widget1.setLayout(self.charLayout)
        charContain = QScrollArea()
        charContain.setWidget(widget1)
        dataLayout.addWidget(charContain, 1)

        # Finalizing all the work that has been done on the Layout
        self.setLayout(Layout)

    def updateList(self, name = None):
        """
        A function called by the Refresh button and after creating a character.
        This updates the list of characters and resorts it.
        """
        self.charList.clear()
        self.charList.addItems(PlayersDB.listPlayers())
        self.charList.sortItems()
        if name:
            item = self.charList.findItems(name, Qt.MatchExactly)
            self.charList.setCurrentItem(item[0])

    def changePlayer(self):
        """
        When we click on another character on the list, it changes the stats
        displayed.
        """
        if self.charList.currentItem() == None:
            self.charLayout.showPlayer()
            return
        self.charLayout.showPlayer(Player(self.charList.currentItem().text()))

    class CharCreation(QDialog):
        """
        This class is a dialog that presents a character creation window. This
        window has a cancel button to make it disappear and a save button that will
        write the new character to the databases.
    
        Currently, if not all fields are full, the save will not work, but this
        should be changed with the addition of the character edition feature.
        """
    
        def __init__(self, parent = None):
            super(CharView.CharCreation, self).__init__(parent)
            self.setProperties()
    
        def setProperties(self):
            """
            This function sets the basic properties of the character creation sheet.
            """
            self.setWindowTitle('Character Creation')
    
            savebtn = QPushButton('Save')
            savebtn.clicked.connect(self.saveNew)
            self.label = QLabel('')
    
            closebtn = QPushButton('Cancel')
            closebtn.clicked.connect(self.close)
    
            #menu = QMenu('menu', self)
            Layout = QVBoxLayout()
            self.sheet = CharSheetCreation()
            Layout.addLayout(self.sheet)
    
            btnLayout = QHBoxLayout()
            btnLayout.addWidget(closebtn)
            btnLayout.addStretch()
            btnLayout.addWidget(self.label)
            btnLayout.addWidget(savebtn)
    
            Layout.addLayout(btnLayout)
    
            self.setLayout(Layout)
    
        def saveNew(self):
            """
            Saves a new character to the database.
            """
            # Making a description table
            description = []
            if self.sheet.player().text() == '':
                self.label.setText('Please enter a player name.')
                return
            else:
                description.append(self.sheet.player().text())
    
            if self.sheet.name().text() == '':
                self.label.setText('Please enter a character name.')
                return
            else:
                description.append(self.sheet.name().text())
    
            description.append(self.sheet.race().currentText())
            
            try:
                description.append(int(self.sheet.level().currentText()))
            except ValueError as e:
                self.label.setText('Level has to be an integer number.')
                return
    
            description.append(self.sheet.align().currentText())
    
            description.append(self.sheet.deity().currentText())
    
            description.append(self.sheet.size().currentText())
            
            description.append(self.sheet.gender().text())
    
            try:
                description.append(int(self.sheet.age().text()))
            except ValueError as e:
                self.label.setText('Age has to be an integer number.')
                return
    
            try:
                description.append(float(self.sheet.height().text()))
            except ValueError as e:
                self.label.setText('Height has to be an number in meters.')
                return
            
            try:
                description.append(float(self.sheet.weight().text()))
            except ValueError as e:
                self.label.setText('Weight has to be an number in kg.')
                return
            
            description.append(self.sheet.hair().text())
    
            description.append(self.sheet.eye().text())
    
            # Making the attributes table
            attributes = []
            attributes.append(int(self.sheet.STR().text()))
            attributes.append(int(self.sheet.CON().text()))
            attributes.append(int(self.sheet.WIS().text()))
            attributes.append(int(self.sheet.DEX().text()))
            attributes.append(int(self.sheet.INT().text()))
            attributes.append(int(self.sheet.CHA().text()))
            try:
                Player.newCharacter(description, attributes)
            except NameError as e:
                self.label.setText(str(e))
                return
            self.close()

    def addChar(self):
        """
        A function that shows a window to create a character.
        """
        newChar = CharView.CharCreation(self)
        newChar.exec()
        self.updateList()

    def updateChar(self):
        """
        Changes the widgets to allow the modification of the caracter.
        """
        if not self.charList.currentItem():
            return
        # Changing the update button
        self.charLayout.updateSwitch()
        self.charList.setEnabled(False)
        self.addbtn.setEnabled(False)
        self.refreshbtn.setEnabled(False)

        self.update.setCurrentIndex(1)

    def saveUpdate(self):
        """
        This pops a confirmation box that allows the user to save the changes
        he made, to revert them back or to continue editing.
        Depending on the user choice, it is then exectuted.
        """
        name = self.charList.currentItem().text()
        # Checking if we are done with the changes
        dialog = QMessageBox()
        dialog.setText('Do you want to save what has been changed?')
        dialog.addButton('Of course', QMessageBox.AcceptRole)
        dialog.addButton('Not done yet', QMessageBox.RejectRole)
        dialog.addButton('I want my mommy to clear my mess', QMessageBox.DestructiveRole)
        dialog.exec()
        if dialog.result() == 1:
            return
        if dialog.result() == 0:
            name = self.pushChanges()
            if name == None:
                return

        self.label.setText('')
        self.charLayout.updateSwitch()
        # Remaking stuff non editable
        self.charList.setEnabled(True)
        self.addbtn.setEnabled(True)
        self.refreshbtn.setEnabled(True)
        self.update.setCurrentIndex(0)
        self.updateList(name)

    def deleteChar(self):
        """
        This deletes prompts the user to delete the currently selected character.
        """
        if not self.charList.currentItem():
            return
        box = QMessageBox()
        box.setText('This will delete ' + self.charList.currentItem().text()
                + "'s character.")
        box.setInformativeText('Are you sure?')
        box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        box.setDefaultButton(QMessageBox.Cancel)
        result = box.exec()
        if result == QMessageBox.Ok:
            PlayersDB.removePlayer(self.charList.currentItem().text())
        self.updateList()

    def pushChanges(self):
        """
        This function stores changes made to the character into the database.
        """
        oldName = self.charList.currentItem().text()
        nameChanged = None
        if self.charList.currentItem().text() == self.charLayout.player().text():
            nameChanged = False
        else:
            nameChanged = True

        # Making a description table
        description = []
        if self.charLayout.player().text() == '':
            self.label.setText('Please enter a player name.')
            return None
        else:
            description.append(self.charLayout.player().text())
        if self.charLayout.name().text() == '':
            self.label.setText('Please enter a character name.')
            return None
        else:
            description.append(self.charLayout.name().text())
        if self.charLayout.race().currentIndex() == 0:
            self.charLayout.race().setCurrentIndex(1)
            description.append(self.charLayout.race().currentWidget().currentText())
            self.charLayout.race().setCurrentIndex(0)
            self.charLayout.level().setCurrentIndex(1)
            description.append(int(self.charLayout.level().currentWidget().currentText()))
            self.charLayout.level().setCurrentIndex(0)
        else:
            description.append(self.charLayout.race().currentWidget().currentText())
            description.append(int(self.charLayout.level().currentWidget().currentText()))
        description.append(self.charLayout.align().currentWidget().currentText())
        description.append(self.charLayout.deity().currentWidget().currentText())
        description.append(self.charLayout.size().currentWidget().currentText())
        description.append(self.charLayout.gender().text())
        try:
            description.append(int(self.charLayout.age().text()))
        except ValueError as e:
            self.label.setText('Age has to be an integer number.')
            return None
        try:
            description.append(float(self.charLayout.height().text()))
        except ValueError as e:
            self.label.setText('Height has to be an number in meters.')
            return None
        try:
            description.append(float(self.charLayout.weight().text()))
        except ValueError as e:
            self.label.setText('Weight has to be an number in kg.')
            return None
        description.append(self.charLayout.hair().text())
        description.append(self.charLayout.eye().text())

        # Making the attributes table
        attributes = []
        if self.charLayout.STR().currentIndex() == 0:
            attributes.append(int(self.charLayout.STR().currentWidget().text()))
            attributes.append(int(self.charLayout.CON().currentWidget().text()))
            attributes.append(int(self.charLayout.WIS().currentWidget().text()))
            attributes.append(int(self.charLayout.DEX().currentWidget().text()))
            attributes.append(int(self.charLayout.INT().currentWidget().text()))
            attributes.append(int(self.charLayout.CHA().currentWidget().text()))
        else:
            attributes.append(self.charLayout.STR().currentWidget().value())
            attributes.append(self.charLayout.CON().currentWidget().value())
            attributes.append(self.charLayout.WIS().currentWidget().value())
            attributes.append(self.charLayout.DEX().currentWidget().value())
            attributes.append(self.charLayout.INT().currentWidget().value())
            attributes.append(self.charLayout.CHA().currentWidget().value())

        skills = {}
        for skill in SkillData.Skills:
            skills[skill] = self.charLayout.skillLayout.checkProf(skill)

        if nameChanged:
            try:
                Player.newCharacter(description, attributes, skills)
            except NameError as e:
                self.label.setText(str(e))
                return None
            PlayersDB.removePlayer(oldName) 
        else:
            player = Player(oldName)
            player.description = Description(description[1:])
            player.attributes = Attributes(attributes)
            player.skills = Skills(skills)
            player.updateChar()
        return description[0]
