# PyQt imports
from PyQt5.QtWidgets import (QTabWidget, QDesktopWidget)
# GUI imports
from GUI.Tabs import *

class GUI(QTabWidget):

    def __init__(self):
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This function sets the basic properties of the character creation sheet.
        """
        self.setWindowTitle('Description setup')
        #self.addTab(CharCreation(), 'Character Creation')
        self.addTab(CharView(), 'Characters')
        self.addTab(Dice(), 'Dice Rolls')

        # Centers the window on the screen
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

        #self.show()
