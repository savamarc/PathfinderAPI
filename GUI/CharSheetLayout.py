from data import *
from PyQt5.QtWidgets import (QBoxLayout, QFormLayout, QLabel, QLineEdit,
        QVBoxLayout, QStackedWidget, QSpinBox, QComboBox, QHBoxLayout,
        QRadioButton, QButtonGroup, QGridLayout, QCheckBox)
from PyQt5.QtCore import Qt, pyqtSignal

"""
This file holds lots of boring ugly code. This basically sets up widgets for the
rest of the code to work with.

Design choices:
    It could be possible to use subclasses of the CharSheetLayout class instead
    of defining a function for each case that we will use, but I opted for this
    strategy because of the expandability that we get. Basically, every layout
    for each part of the character sheet is completely independant. This feels
    very important to me because adding new parts of the character sheet will
    not get more complicated as I progress. This is not true if I implement a
    class for each use of the CharSheetLayout class. Instead, each of those will
    become messier with each expansion and contain code very similar to the 
    other while still slightly different. 
"""

class CharSheetLayout(QBoxLayout):
    """
    This is the layout of a character sheet.
    This basis is intended to be easy to module. It contains many fields that
    are the different parts of a character sheet, and it places them in a
    certain way when shown. This way does not changes, but the different parts
    of the sheet can be moduled easily. The functions to change the type of 
    character sheet have to be called manually.
    """

    def __init__(self):
        super().__init__(QBoxLayout.LeftToRight)
        self.setProperties()

    def setProperties(self):
        """
        This function sets the basic properties of the character viewing sheet.
        """
        # Adding a the description
        self.descrLayout = DescriptionLayout()
        self.addLayout(self.descrLayout)

        # Adding the attributes
        self.attrLayout = AttributesLayout()
        self.addLayout(self.attrLayout)

        # Adding the skills
        self.skillLayout = SkillsLayout()
        self.addLayout(self.skillLayout)

    def makeUpdate(self):
        """
        This function transforms the character sheet into an updatable one.
        """
        self.attrLayout.makeModifiable()
        self.descrLayout.makeModifiable()
        self.skillLayout.makeModifiable()

    def makeCreation(self):
        """
        This function transforms the character sheet into the format that is
        used to create a character.
        """
        self.attrLayout.makeCreation()
        self.descrLayout.makeCreation()
        self.skillLayout.makeCreation()

    # First are the getter for the description
    def player(self):
        return self.descrLayout.player
    def name(self):
        return self.descrLayout.name
    def race(self):
        return self.descrLayout.race
    def role(self):
        return self.descrLayout.role
    def level(self):
        return self.descrLayout.level
    def align(self):
        return self.descrLayout.align
    def deity(self):
        return self.descrLayout.deity
    def size(self):
        return self.descrLayout.size
    def gender(self):
        return self.descrLayout.gender
    def age(self):
        return self.descrLayout.age
    def height(self):
        return self.descrLayout.height
    def weight(self):
        return self.descrLayout.weight
    def hair(self):
        return self.descrLayout.hair
    def eye(self):
        return self.descrLayout.eye
    # We then make getter for the attributes
    def STR(self):
        return self.attrLayout.STR
    def CON(self):
        return self.attrLayout.CON
    def WIS(self):
        return self.attrLayout.WIS
    def DEX(self):
        return self.attrLayout.DEX
    def INT(self):
        return self.attrLayout.INT
    def CHA(self):
        return self.attrLayout.CHA
    def STRmod(self):
        return self.attrLayout.STRmod
    def CONmod(self):
        return self.attrLayout.CONmod
    def WISmod(self):
        return self.attrLayout.WISmod
    def DEXmod(self):
        return self.attrLayout.DEXmod
    def INTmod(self):
        return self.attrLayout.INTmod
    def CHAmod(self):
        return self.attrLayout.CHAmod

class DescriptionLayout(QFormLayout):
    """
    This is the layout of the description of a character.
    """
    statsFieldChange = pyqtSignal()

    def __init__(self):
        """
        This just sets the initial layout. To change the widgets for the
        different use cases, you must implement a function and call it in the
        CharSheetLayout reimplementation that you use.
        """
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This places the basic widgets in a form layout.
        """
        player = QLabel('Player name')
        self.player = QLineEdit()
        self.player.setReadOnly(True)
        self.addRow(player, self.player)
        name = QLabel('Character name')
        self.name = QLineEdit()
        self.name.setReadOnly(True)
        self.addRow(name, self.name)
        race = QLabel('Race')
        self.race = QLineEdit()
        self.race.setReadOnly(True)
        self.addRow(race, self.race)
        role = QLabel('Class')
        self.role = QLineEdit()
        self.role.setReadOnly(True)
        self.addRow(role, self.role)
        level = QLabel('Level')
        self.level = QLineEdit()
        self.level.setReadOnly(True)
        self.addRow(level, self.level)
        align = QLabel('Alignment')
        self.align = QLineEdit()
        self.align.setReadOnly(True)
        self.addRow(align, self.align)
        deity = QLabel('Deity')
        self.deity = QLineEdit()
        self.deity.setReadOnly(True)
        self.addRow(deity, self.deity)
        size = QLabel('Size')
        self.size = QLineEdit()
        self.size.setReadOnly(True)
        self.addRow(size, self.size)
        gender = QLabel('Gender')
        self.gender = QLineEdit()
        self.gender.setReadOnly(True)
        self.addRow(gender, self.gender)
        age = QLabel('Age')
        self.age = QLineEdit()
        self.age.setReadOnly(True)
        self.addRow(age, self.age)
        height = QLabel('Height')
        self.height = QLineEdit()
        self.height.setReadOnly(True)
        self.addRow(height, self.height)
        weight = QLabel('Weight')
        self.weight = QLineEdit()
        self.weight.setReadOnly(True)
        self.addRow(weight, self.weight)
        hair = QLabel('Hair color')
        self.hair = QLineEdit()
        self.hair.setReadOnly(True)
        self.addRow(hair, self.hair)
        eye = QLabel('Eye color')
        self.eye = QLineEdit()
        self.eye.setReadOnly(True)
        self.addRow(eye, self.eye)

    def makeModifiable(self):
        """
        Called by the constructor to change the fields of the widget to make
        them modifable on demand.
        Watch out as the QStackedWidget defined here work differently than the
        the previous field.
        """
        tmp = self.align
        tmp.setParent(None)
        self.align = QStackedWidget()
        self.align.addWidget(tmp)
        choice = QComboBox()
        choice.addItems(Constants.Alignments)
        self.align.addWidget(choice)
        self.replaceWidget(tmp, self.align)
        
        tmp = self.deity
        tmp.setParent(None)
        self.deity = QStackedWidget()
        self.deity.addWidget(tmp)
        choice = QComboBox()
        choice.addItems(Constants.Deities)
        self.deity.addWidget(choice)
        self.replaceWidget(tmp, self.deity)

        tmp = self.size
        tmp.setParent(None)
        self.size = QStackedWidget()
        self.size.addWidget(tmp)
        choice = QComboBox()
        choice.addItems(Constants.Sizes)
        self.size.addWidget(choice)
        self.replaceWidget(tmp, self.size)
        
        tmp = self.race
        tmp.setParent(None)
        self.race = QStackedWidget()
        self.race.addWidget(tmp)
        choice = QComboBox()
        choice.addItems(Constants.Races)
        self.race.addWidget(choice)
        self.replaceWidget(tmp, self.race)
        
        tmp = self.level
        tmp.setParent(None)
        self.level = QStackedWidget()
        self.level.addWidget(tmp)
        choice = QComboBox()
        for i in range(20):
            choice.addItem(str(i+1))
        self.level.addWidget(choice)
        self.replaceWidget(tmp, self.level)

    def makeCreation(self):
        """
        Called by the constructor to change the fields of the widget to make
        this the layout of a creation sheet.
        """
        # Making fields editable
        self.player.setReadOnly(False)
        self.name.setReadOnly(False)
        self.gender.setReadOnly(False)
        self.age.setReadOnly(False)
        self.height.setReadOnly(False)
        self.weight.setReadOnly(False)
        self.hair.setReadOnly(False)
        self.eye.setReadOnly(False)
        
        # Making combo box fields for options for which we need to choose from.
        tmp = self.race
        self.race = QComboBox()
        self.race.addItems(Constants.Races)
        self.replaceWidget(tmp, self.race)
        self.race.currentIndexChanged.connect(self.statsFieldChange)

        tmp = self.role
        self.role = QComboBox()
        self.role.addItems(Constants.Classes)
        self.replaceWidget(tmp, self.role)
        self.role.currentIndexChanged.connect(self.statsFieldChange)

        tmp = self.level
        self.level = QComboBox()
        for i in range(1, 21):
            self.level.addItem(str(i))
        self.replaceWidget(tmp, self.level)
        self.level.currentIndexChanged.connect(self.statsFieldChange)

        tmp = self.align
        self.align = QComboBox()
        self.align.addItems(Constants.Alignments)
        self.replaceWidget(tmp, self.align)
        
        tmp = self.deity
        self.deity = QComboBox()
        self.deity.addItems(Constants.Deities)
        self.replaceWidget(tmp, self.deity)

        tmp = self.size
        self.size = QComboBox()
        self.size.addItems(Constants.Sizes)
        self.replaceWidget(tmp, self.size)

class AttributesLayout(QFormLayout):
    """
    This is the layout of the description of a character.

    This works the same as DescriptionLayout
    """
    attributeChanged = pyqtSignal(int, int)

    def __init__(self):
        """
        This just sets the initial layout. To change the widgets for the
        different use cases, you must implement a function and call it in the
        CharSheetLayout reimplementation that you use.
        """
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This places the basic widgets in a form layout.
        """
        STR         = QLabel("STR")
        self.STR    = QLineEdit()
        self.STR.setReadOnly(True)
        self.STRmod = QLineEdit()
        self.STRmod.setReadOnly(True)
        STRLayout   = QVBoxLayout()
        STRLayout.addWidget(self.STR)
        STRLayout.addWidget(self.STRmod)
        self.addRow(STR, STRLayout)
        CON         = QLabel('CON')
        self.CON    = QLineEdit()
        self.CON.setReadOnly(True)
        self.CONmod = QLineEdit()
        self.CONmod.setReadOnly(True)
        CONLayout   = QVBoxLayout()
        CONLayout.addWidget(self.CON)
        CONLayout.addWidget(self.CONmod)
        self.addRow(CON, CONLayout)
        WIS         = QLabel('WIS')
        self.WIS    = QLineEdit()
        self.WIS.setReadOnly(True)
        self.WISmod = QLineEdit()
        self.WISmod.setReadOnly(True)
        WISLayout   = QVBoxLayout()
        WISLayout.addWidget(self.WIS)
        WISLayout.addWidget(self.WISmod)
        self.addRow(WIS, WISLayout)
        DEX         = QLabel('DEX')
        self.DEX    = QLineEdit()
        self.DEX.setReadOnly(True)
        self.DEXmod = QLineEdit()
        self.DEXmod.setReadOnly(True)
        DEXLayout   = QVBoxLayout()
        DEXLayout.addWidget(self.DEX)
        DEXLayout.addWidget(self.DEXmod)
        self.addRow(DEX, DEXLayout)
        INT         = QLabel('INT')
        self.INT    = QLineEdit()
        self.INT.setReadOnly(True)
        self.INTmod = QLineEdit()
        self.INTmod.setReadOnly(True)
        INTLayout   = QVBoxLayout()
        INTLayout.addWidget(self.INT)
        INTLayout.addWidget(self.INTmod)
        self.addRow(INT, INTLayout)
        CHA         = QLabel('CHA')
        self.CHA    = QLineEdit()
        self.CHA.setReadOnly(True)
        self.CHAmod = QLineEdit()
        self.CHAmod.setReadOnly(True)
        CHALayout   = QVBoxLayout()
        CHALayout.addWidget(self.CHA)
        CHALayout.addWidget(self.CHAmod)
        self.addRow(CHA, CHALayout)

    def makeModifiable(self):
        """
        Called by the constructor to change the fields of the widget to make
        them modifable on demand.
        Watch out as the QStackedWidget defined here work differently than the
        the previous field.
        """
        tmp = self.STR
        tmp.setParent(None)
        self.STR = QStackedWidget()
        self.STR.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.STR.addWidget(choice)
        self.replaceWidget(tmp, self.STR)
        
        tmp = self.CON
        tmp.setParent(None)
        self.CON = QStackedWidget()
        self.CON.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.CON.addWidget(choice)
        self.replaceWidget(tmp, self.CON)
        
        tmp = self.WIS
        tmp.setParent(None)
        self.WIS = QStackedWidget()
        self.WIS.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.WIS.addWidget(choice)
        self.replaceWidget(tmp, self.WIS)
        
        tmp = self.DEX
        tmp.setParent(None)
        self.DEX = QStackedWidget()
        self.DEX.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.DEX.addWidget(choice)
        self.replaceWidget(tmp, self.DEX)
        
        tmp = self.INT
        tmp.setParent(None)
        self.INT = QStackedWidget()
        self.INT.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.INT.addWidget(choice)
        self.replaceWidget(tmp, self.INT)
        
        tmp = self.CHA
        tmp.setParent(None)
        self.CHA = QStackedWidget()
        self.CHA.addWidget(tmp)
        choice = QSpinBox()
        choice.setMinimum(1)
        self.CHA.addWidget(choice)
        self.replaceWidget(tmp, self.CHA)

    def makeCreation(self):
        """
        Called by the constructor to change the fields of the widget to make
        this the layout of a creation sheet.
        """
        # Making spinbox fields for Attributes
        tmp = self.STR
        self.STR = QSpinBox()
        self.STR.lineEdit().setReadOnly(True)
        self.STR.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 0))
        self.replaceWidget(tmp, self.STR)
        self.STR.setValue(10)

        tmp = self.CON
        self.CON = QSpinBox()
        self.CON.lineEdit().setReadOnly(True)
        self.CON.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 1))
        self.CON.setValue(10)
        self.replaceWidget(tmp, self.CON)

        tmp = self.WIS
        self.WIS = QSpinBox()
        self.WIS.lineEdit().setReadOnly(True)
        self.WIS.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 2))
        self.WIS.setValue(10)
        self.replaceWidget(tmp, self.WIS)

        tmp = self.DEX
        self.DEX = QSpinBox()
        self.DEX.lineEdit().setReadOnly(True)
        self.DEX.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 3))
        self.DEX.setValue(10)
        self.replaceWidget(tmp, self.DEX)

        tmp = self.INT
        self.INT = QSpinBox()
        self.INT.lineEdit().setReadOnly(True)
        self.INT.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 4))
        self.INT.setValue(10)
        self.replaceWidget(tmp, self.INT)

        tmp = self.CHA
        self.CHA = QSpinBox()
        self.CHA.lineEdit().setReadOnly(True)
        self.CHA.valueChanged.connect(lambda x: self.attributeChanged.emit(x, 5))
        self.CHA.setValue(10)
        self.replaceWidget(tmp, self.CHA)

class SkillsLayout(QGridLayout):
    """
    This is the layout of the description of a character. It has a few various
    modes depending on wheter you want the layout to be modifiable or not.

    This works the same as DescriptionLayout
    """

    class SkillLayout(QHBoxLayout):
        """
        This contains all the widgets that represent a specific skill in a 
        """
        def __init__(self, name):
            super().__init__()
            self.name = name
            self.namelabel = QLabel(name)
            # Defining the elements a single skill
            self.bonus = QSpinBox()
            self.bonus.lineEdit().setReadOnly(True)
            self.bonus.setButtonSymbols(2)
            self.sigbtn = QCheckBox()
            self.attr = QLabel(SkillData.Skills[name][0])
            self.attrmod = QLabel('')
            self.ubtn = QRadioButton()
            self.tbtn = QRadioButton()
            self.ebtn = QRadioButton()
            self.mbtn = QRadioButton()
            self.lbtn = QRadioButton()
            self.prof = QButtonGroup()
            self.prof.addButton(self.ubtn, 0)
            self.prof.addButton(self.tbtn, 1)
            self.prof.addButton(self.ebtn, 2)
            self.prof.addButton(self.mbtn, 3)
            self.prof.addButton(self.lbtn, 4)
            self.profmod = QSpinBox()
            self.profmod.lineEdit().setReadOnly(True)
            self.profmod.setButtonSymbols(2)
            self.othermod = QSpinBox()
            self.othermod.lineEdit().setReadOnly(True)
            self.othermod.setButtonSymbols(2)

        def toggleProfBtn(self, which):
            """
            This toggles the button associated with the proficiency which.
            This also changes the value of the profmod box
            """
            if which == 'U':
                self.ubtn.setCheckable(True)
                self.ubtn.setChecked(True)
                self.profmod.setMinimum(Constants.ProfValues[which])
                self.profmod.setMaximum(Constants.ProfValues[which])
                self.tbtn.setCheckable(False)
                self.ebtn.setCheckable(False)
                self.mbtn.setCheckable(False)
                self.lbtn.setCheckable(False)
            elif which == 'T':
                self.ubtn.setCheckable(False)
                self.tbtn.setCheckable(True)
                self.tbtn.setChecked(True)
                self.profmod.setMinimum(Constants.ProfValues[which])
                self.profmod.setMaximum(Constants.ProfValues[which])
                self.ebtn.setCheckable(False)
                self.mbtn.setCheckable(False)
                self.lbtn.setCheckable(False)
            elif which == 'E':
                self.ubtn.setCheckable(False)
                self.tbtn.setCheckable(False)
                self.ebtn.setCheckable(True)
                self.ebtn.setChecked(True)
                self.profmod.setMinimum(Constants.ProfValues[which])
                self.profmod.setMaximum(Constants.ProfValues[which])
                self.mbtn.setCheckable(False)
                self.lbtn.setCheckable(False)
            elif which == 'M':
                self.ubtn.setCheckable(False)
                self.tbtn.setCheckable(False)
                self.ebtn.setCheckable(False)
                self.mbtn.setCheckable(True)
                self.mbtn.setChecked(True)
                self.profmod.setMinimum(Constants.ProfValues[which])
                self.profmod.setMaximum(Constants.ProfValues[which])
                self.lbtn.setCheckable(False)
            elif which == 'L':
                self.ubtn.setCheckable(False)
                self.tbtn.setCheckable(False)
                self.ebtn.setCheckable(False)
                self.mbtn.setCheckable(False)
                self.lbtn.setCheckable(True)
                self.lbtn.setChecked(True)
                self.profmod.setMinimum(Constants.ProfValues[which])
                self.profmod.setMaximum(Constants.ProfValues[which])
            elif which == 'None':
                self.ubtn.setCheckable(False)
                self.tbtn.setCheckable(False)
                self.ebtn.setCheckable(False)
                self.mbtn.setCheckable(False)
                self.lbtn.setCheckable(False)
                self.profmod.setMinimum(0)
                self.profmod.setMaximum(0)

        #def getToggled(self):
        #    """
        #    This returns the letter representing the proficiency associated with
        #    this skill.
        #    """
        #    x = self.checkedId()
        #    if x == 0:
        #        return 'U'
        #    if x == 1:
        #        return 'T'
        #    if x == 2:
        #        return 'E'
        #    if x == 3:
        #        return 'M'
        #    if x == 4:
        #        return 'L'

    def __init__(self):
        """
        Just the usual.
        """
        super().__init__()
        self.setProperties()

    def setProperties(self):
        """
        This places the basic widgets in a form layout.
        """
        self.skills = {}
        row = 0
        for x in SkillData.Skills:
            self.skills[x] = self.SkillLayout(x)
            self.addWidget(self.skills[x].namelabel, 2*row, 0)
            self.addWidget(self.skills[x].bonus, 2*row+1, 0)
            self.addWidget(QLabel('SIG'), 2*row, 1)
            self.addWidget(self.skills[x].sigbtn, 2*row+1, 1)
            self.addWidget(self.skills[x].attr, 2*row, 2)
            self.addWidget(self.skills[x].attrmod, 2*row +1, 2)
            self.addWidget(QLabel('U'), 2*row, 3)
            self.addWidget(QLabel('T'), 2*row, 4)
            self.addWidget(QLabel('E'), 2*row, 5)
            self.addWidget(QLabel('M'), 2*row, 6)
            self.addWidget(QLabel('L'), 2*row, 7)
            self.addWidget(self.skills[x].ubtn, 2*row+1, 3)
            self.addWidget(self.skills[x].tbtn, 2*row+1, 4)
            self.addWidget(self.skills[x].ebtn, 2*row+1, 5)
            self.addWidget(self.skills[x].mbtn, 2*row+1, 6)
            self.addWidget(self.skills[x].lbtn, 2*row+1, 7)
            self.skills[x].toggleProfBtn('None')
            self.addWidget(QLabel('PROF'), 2*row, 8)
            self.addWidget(self.skills[x].profmod, 2*row+1, 8)
            self.addWidget(QLabel('OTHER'), 2*row, 9)
            self.addWidget(self.skills[x].othermod, 2*row+1, 9)
            row += 1

    def makeModifiable(self):
        """
        This does nothing because modifying the skills is tied to the master
        mode.
        """
        pass

    def makeCreation(self):
        """
        Called by the constructor to change the fields of the widget to make
        this the layout of a creation sheet. This makes all buttons clickable
        and clicks on untrained for all skills.
        """
        for x in self.skills:
            self.skills[x].toggleProfBtn('U')
            self.skills[x].tbtn.setCheckable(True)
            self.skills[x].ebtn.setCheckable(True)
            self.skills[x].mbtn.setCheckable(True)
            self.skills[x].lbtn.setCheckable(True)

    def checkProf(self, skill):
        """
        Takes the skill name `skill` and returns the proficiency presently shown
        in the UI for this skill.
        """
        if self.skills[skill].ubtn.isChecked():
            return 'U'
        elif self.skills[skill].tbtn.isChecked():
            return 'T'
        elif self.skills[skill].ebtn.isChecked():
            return 'E'
        elif self.skills[skill].mbtn.isChecked():
            return 'M'
        elif self.skills[skill].lbtn.isChecked():
            return 'L'
        return 'U'
