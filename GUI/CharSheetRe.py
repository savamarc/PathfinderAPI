"""
This module contains reimplementations of the CharSheetLayout class.
"""

from data import *
from GUI.CharSheetLayout import CharSheetLayout
from PyQt5.QtWidgets import (QComboBox, QSpinBox, QLabel, QVBoxLayout,
        QStackedWidget, QPushButton, QSizePolicy, QMessageBox)

class CharSheetCreation(CharSheetLayout):
    """
    This is a modified version of the layout of a character sheet for the case
    where we want to create a character. What happens in that case is simply
    that options that require choosing between multiple choices a changed to
    QComboBox.
    I know attribute takes two t's.
    """

    def __init__(self):
        super().__init__()
        self.makeCreation()
        self.changeProperties()
        self.resetScores()

    def changeProperties(self):
        """
        This function changes the widgets used in the character sheet layout
        so that they are editable and/or offer ways to instanciate all the
        features of a character.
        """
        self.descrLayout.statsFieldChange.connect(self.resetScores)
        self.attrLayout.attributeChanged.connect(lambda x,y: self.changeATR(x, y))

        raceScores = QLabel('Race ability scores left')
        self.raceScores = QLabel('0')
        self.attrLayout.addRow(raceScores, self.raceScores)
        levelScores = QLabel('Level ability scores left')
        self.levelScores = QLabel('0')
        self.attrLayout.addRow(levelScores, self.levelScores)
        self.levelScoreCap = 0
        # Putting stuff in lists for the modification function
        self.modLabels = [self.STRmod(), self.CONmod(), self.WISmod(),
                self.DEXmod(), self.INTmod(), self.CHAmod()]
        self.atrRaceScore = []
        self.atrLevelScore = []
        self.atrOld = []
        # 0:STR, 1:CON, 2:WIS, 3:DEX, 4:INT, 5:CHA
        for i in range(12):
            self.atrRaceScore.append(0)
            self.atrLevelScore.append(0)
            self.atrOld.append(0)
        self.spinBoxes = [self.STR(), self.CON(), self.WIS(), self.DEX(),
                self.INT(), self.CHA()]

    def changeATR(self, new, index):
        """
        This changes the value of ATR according to the rules of PF2 with ability boosts.
        Note that stepUp() and stepDown() call this function recursively.
        """
        levelScores = int(self.levelScores.text())
        raceScores = int(self.raceScores.text())
        if self.atrOld[index] == 0:
            self.modLabels[index].setText('+' + str((new-10)//2) if (new-10)//2 > 0 else str((new-10)//2))
            self.atrOld[index] = new
        if new == self.atrOld[index]:
            return
        if levelScores == 0 and new > self.atrOld[index]:
            self.spinBoxes[index].stepDown()
            return
        if self.atrLevelScore[index] >= self.levelScoreCap and new > self.atrOld[index]:
            self.spinBoxes[index].stepDown()
            return
        if raceScores and self.atrRaceScore[index] and new > self.atrOld[index]:
            self.spinBoxes[index].stepDown()
            return
        if new > self.atrOld[index] and (new-self.atrOld[index]) < 2:
            if new < 18:
                self.spinBoxes[index].stepUp()
                return
        if new < self.atrOld[index] and (self.atrOld[index]-new) < 2:
            if new < 18:
                self.spinBoxes[index].stepDown()
                return
        self.modLabels[index].setText('+' + str((new-10)//2) if (new-10)//2 > 0 else str((new-10)//2))
        if new > self.atrOld[index]:
            if raceScores:
                self.raceScores.setText(str(raceScores-1))
                self.atrRaceScore[index] += 1
            else:
                self.atrLevelScore[index] += 1
                self.levelScores.setText(str(levelScores-1))
        elif new < self.atrOld[index]:
            if self.atrLevelScore[index]:
                self.atrLevelScore[index] -= 1
                self.levelScores.setText(str(levelScores+1))
            else:
                self.atrRaceScore[index] -= 1
                self.raceScores.setText(str(raceScores+1))
        self.atrOld[index] = new

    def resetScores(self):
        """
        Reseting the ability scores. This applies the racial bonuses, then the level, and finally the class.
        """
        self.STR().setMinimum(1)
        self.CON().setMinimum(1)
        self.WIS().setMinimum(1)
        self.DEX().setMinimum(1)
        self.INT().setMinimum(1)
        self.CHA().setMinimum(1)
        self.STR().setValue(1)
        self.CON().setValue(1)
        self.WIS().setValue(1)
        self.DEX().setValue(1)
        self.INT().setValue(1)
        self.CHA().setValue(1)
        for i in range(6):
            self.atrOld[i] = 0
            self.atrRaceScore[i] = 0
            self.atrLevelScore[i] = 0
        race = self.race().currentText()
        level = int(self.level().currentText())
        role = self.role().currentText()
        self.levelScores.setText(str((level//5 + 1)*4))
        self.levelScoreCap = level//5+1
        if race == 'Dwarf':
            self.STR().setMinimum(10)
            self.CON().setMinimum(12)
            self.WIS().setMinimum(12)
            self.DEX().setMinimum(10)
            self.INT().setMinimum(10)
            self.CHA().setMinimum(8)
            self.STR().setValue(10)
            self.CON().setValue(12)
            self.WIS().setValue(12)
            self.DEX().setValue(10)
            self.INT().setValue(10)
            self.CHA().setValue(8)
            self.raceScores.setText('1')
        if race == 'Elf':
            self.STR().setMinimum(10)
            self.CON().setMinimum(8)
            self.WIS().setMinimum(10)
            self.DEX().setMinimum(12)
            self.INT().setMinimum(12)
            self.CHA().setMinimum(10)
            self.STR().setValue(10)
            self.CON().setValue(8)
            self.WIS().setValue(10)
            self.DEX().setValue(12)
            self.INT().setValue(12)
            self.CHA().setValue(10)
            self.raceScores.setText('1')
        if race == 'Gnome':
            self.STR().setMinimum(8)
            self.CON().setMinimum(12)
            self.WIS().setMinimum(10)
            self.DEX().setMinimum(10)
            self.INT().setMinimum(10)
            self.CHA().setMinimum(12)
            self.STR().setValue(8)
            self.CON().setValue(12)
            self.WIS().setValue(10)
            self.DEX().setValue(10)
            self.INT().setValue(10)
            self.CHA().setValue(12)
            self.raceScores.setText('1')
        if race == 'Goblin':
            self.STR().setMinimum(10)
            self.CON().setMinimum(10)
            self.WIS().setMinimum(8)
            self.DEX().setMinimum(12)
            self.INT().setMinimum(10)
            self.CHA().setMinimum(12)
            self.STR().setValue(10)
            self.CON().setValue(10)
            self.WIS().setValue(8)
            self.DEX().setValue(12)
            self.INT().setValue(10)
            self.CHA().setValue(12)
            self.raceScores.setText('1')
        if race == 'Halfling':
            self.STR().setMinimum(8)
            self.CON().setMinimum(10)
            self.WIS().setMinimum(12)
            self.DEX().setMinimum(12)
            self.INT().setMinimum(10)
            self.CHA().setMinimum(10)
            self.STR().setValue(8)
            self.CON().setValue(10)
            self.WIS().setValue(12)
            self.DEX().setValue(12)
            self.INT().setValue(10)
            self.CHA().setValue(10)
            self.raceScores.setText('1')
        if race == 'Human':
            self.STR().setMinimum(10)
            self.CON().setMinimum(10)
            self.WIS().setMinimum(10)
            self.DEX().setMinimum(10)
            self.INT().setMinimum(10)
            self.CHA().setMinimum(10)
            self.STR().setValue(10)
            self.CON().setValue(10)
            self.WIS().setValue(10)
            self.DEX().setValue(10)
            self.INT().setValue(10)
            self.CHA().setValue(10)
            self.raceScores.setText('2')
        if level == 1:
            self.STR().setMaximum(18)
            self.CON().setMaximum(18)
            self.WIS().setMaximum(18)
            self.DEX().setMaximum(18)
            self.INT().setMaximum(18)
            self.CHA().setMaximum(18)
        else:
            self.STR().setMaximum(25)
            self.CON().setMaximum(25)
            self.WIS().setMaximum(25)
            self.DEX().setMaximum(25)
            self.INT().setMaximum(25)
            self.CHA().setMaximum(25)

class CharSheetUpdate(CharSheetLayout):
    """
    This is a version of the sheet layout that can be switched to be editable
    """

    def __init__(self):
        super().__init__()
        self.changeProperties()
        self.state = 0
        self.makeUpdate()

    def changeProperties(self):
        """
        This function changes the widgets used in the character sheet layout
        so that they are editable and/or offer ways to instanciate all the
        features of a character.
        """

        masterLayout = QVBoxLayout()
        masterLayout.addStretch()
        self.masterbtn = QStackedWidget()
        self.masterbtn.addWidget(QLabel(''))
        self.masterbtn.addWidget(QPushButton('Master'))
        self.masterbtn.widget(1).clicked.connect(self.masterMode)
        self.masterbtn.setSizePolicy(QSizePolicy(QSizePolicy.Fixed,
            QSizePolicy.Fixed, QSizePolicy.PushButton))
        masterLayout.addWidget(self.masterbtn)
        self.addLayout(masterLayout)

    def updateSwitch(self):
        self.state = (self.state+1)%2
        if self.state:
            # Making text fields editable
            self.player().setReadOnly(False)
            self.name().setReadOnly(False)
            self.gender().setReadOnly(False)
            self.age().setReadOnly(False)
            self.height().setReadOnly(False)
            self.weight().setReadOnly(False)
            self.hair().setReadOnly(False)
            self.eye().setReadOnly(False)
        else:
            # Making text fields non-editable
            self.player().setReadOnly(True)
            self.name().setReadOnly(True)
            self.gender().setReadOnly(True)
            self.age().setReadOnly(True)
            self.height().setReadOnly(True)
            self.weight().setReadOnly(True)
            self.hair().setReadOnly(True)
            self.eye().setReadOnly(True)

        if self.masterbtn.currentIndex() == self.state:
            self.race().setCurrentIndex(0)
            self.level().setCurrentIndex(0)
            self.STR().setCurrentIndex(0)
            self.CON().setCurrentIndex(0)
            self.WIS().setCurrentIndex(0)
            self.DEX().setCurrentIndex(0)
            self.INT().setCurrentIndex(0)
            self.CHA().setCurrentIndex(0)
        self.masterbtn.setCurrentIndex(self.state)

        self.align().setCurrentIndex(self.state)
        self.size().setCurrentIndex(self.state)
        self.deity().setCurrentIndex(self.state)

        if self.state == 1:
            self.align().currentWidget().setCurrentText(self.align().widget(0).text())
            self.size().currentWidget().setCurrentText(self.size().widget(0).text())
            self.deity().currentWidget().setCurrentText(self.deity().widget(0).text())

    def masterMode(self):
        dialog = QMessageBox()
        dialog.addButton('Ok', QMessageBox.AcceptRole)
        dialog.addButton('Hell no', QMessageBox.RejectRole)
        dialog.setWindowTitle('BEWARE')
        dialog.setText('This will make all fields editable and can cause a mess')
        dialog.exec()
        if dialog.result():
            return
        self.masterbtn.setCurrentIndex(0)
        self.race().setCurrentIndex(1)
        self.level().setCurrentIndex(1)
        self.STR().setCurrentIndex(1)
        self.STR().currentWidget().setValue(int(self.STR().widget(0).text()))
        self.CON().setCurrentIndex(1)
        self.CON().currentWidget().setValue(int(self.CON().widget(0).text()))
        self.WIS().setCurrentIndex(1)
        self.WIS().currentWidget().setValue(int(self.WIS().widget(0).text()))
        self.DEX().setCurrentIndex(1)
        self.DEX().currentWidget().setValue(int(self.DEX().widget(0).text()))
        self.INT().setCurrentIndex(1)
        self.INT().currentWidget().setValue(int(self.INT().widget(0).text()))
        self.CHA().setCurrentIndex(1)
        self.CHA().currentWidget().setValue(int(self.CHA().widget(0).text()))
        self.STRmod().setText('')
        self.CONmod().setText('')
        self.WISmod().setText('')
        self.DEXmod().setText('')
        self.INTmod().setText('')
        self.CHAmod().setText('')
        for x in self.skillLayout.skills:
            self.skillLayout.skills[x].ubtn.setCheckable(True)
            self.skillLayout.skills[x].tbtn.setCheckable(True)
            self.skillLayout.skills[x].ebtn.setCheckable(True)
            self.skillLayout.skills[x].mbtn.setCheckable(True)
            self.skillLayout.skills[x].lbtn.setCheckable(True)

    def showPlayer(self, player = None):
        """
        This function sets this widget to show the character sheet for player
        `player`.
        This has been moved from the Tabs module because this is a function
        specific to this class.
        """
        # No items clears the boxes
        if player == None:
            self.player().setText('')
            self.name().setText('')
            self.race().currentWidget().setText('')
            self.level().currentWidget().setText('')
            self.align().currentWidget().setText('')
            self.deity().currentWidget().setText('')
            self.size().currentWidget().setText('')
            self.gender().setText('')
            self.age().setText('')
            self.height().setText('')
            self.weight().setText('')
            self.hair().setText('')
            self.eye().setText('')
            self.STR().currentWidget().setText('')
            self.STRmod().setText('')
            self.CON().currentWidget().setText('')
            self.CONmod().setText('')
            self.WIS().currentWidget().setText('')
            self.WISmod().setText('')
            self.DEX().currentWidget().setText('')
            self.DEXmod().setText('')
            self.INT().currentWidget().setText('')
            self.INTmod().setText('')
            self.CHA().currentWidget().setText('')
            self.CHAmod().setText('')
            return

        self.player().setText(player.player)
        self.name().setText(player.description.name)
        self.race().currentWidget().setText(player.description.race)
        self.level().currentWidget().setText(str(player.description.level))
        self.align().currentWidget().setText(player.description.alignment)
        self.deity().currentWidget().setText(player.description.deity)
        self.size().currentWidget().setText(player.description.size)
        self.gender().setText(player.description.gender)
        self.age().setText(str(player.description.age))
        self.height().setText(str(player.description.height))
        self.weight().setText(str(player.description.weight))
        self.hair().setText(player.description.hair)
        self.eye().setText(player.description.eye)
        self.STR().currentWidget().setText(str(player.attributes[0]))
        self.STRmod().setText(str(player.attributes.getMod(0)))
        self.CON().currentWidget().setText(str(player.attributes[1]))
        self.CONmod().setText(str(player.attributes.getMod(1)))
        self.WIS().currentWidget().setText(str(player.attributes[2]))
        self.WISmod().setText(str(player.attributes.getMod(2)))
        self.DEX().currentWidget().setText(str(player.attributes[3]))
        self.DEXmod().setText(str(player.attributes.getMod(3)))
        self.INT().currentWidget().setText(str(player.attributes[4]))
        self.INTmod().setText(str(player.attributes.getMod(4)))
        self.CHA().currentWidget().setText(str(player.attributes[5]))
        self.CHAmod().setText(str(player.attributes.getMod(5)))
        self.setSkills(player)

    def setSkills(self, player):
        mods = {'STR':self.STRmod().text(), 'CON':self.CONmod().text(),
                'WIS':self.WISmod().text(), 'DEX':self.DEXmod().text(),
                'INT':self.INTmod().text(), 'CHA':self.CHAmod().text()}
        for x in player.skills:
            skill = self.skillLayout.skills[x[0]]
            skill.attrmod.setText(mods[skill.attr.text()] if int(mods[skill.attr.text()]) < 0 else '+' + mods[skill.attr.text()])
            skill.toggleProfBtn(x[1])
