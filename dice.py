"""
This file contains implementation of functions that throw dice. Each of those
function takes an integer as input, throws that number of dice and returns the
results in an array.
"""

import randomgen
from randomgen import RandomGenerator
gen = RandomGenerator()

def d4(n=1):
    """
    This throws n d4 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(4)+1)
    return a

def d6(n=1):
    """
    This throws n d6 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(6)+1)
    return a

def d8(n=1):
    """
    This throws n d8 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(8)+1)
    return a

def d10(n=1):
    """
    This throws n d10 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(10)+1)
    return a

def d12(n=1):
    """
    This throws n d12 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(12)+1)
    return a

def d20(n=1):
    """
    This throws n d20 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(20)+1)
    return a

def d100(n=1):
    """
    This throws n d100 and returns an array containing the results.
    """
    a = []
    for i in range(n):
        a.append(gen.randint(100)+1)
    return a
