__all__ = ['Player', 'Components']
"""
This package contains everything relating to the player sheets except the
creation logic because this is included in the interface.
The current goal is for the interface to be robust and for the back-end to just
work. This allows me to not have a bazillion checks in the functions of this
package.
"""
