from data import *
from Player.Components import *
import sqlite3
import os
from sqlite3 import Error

"""
Each time something will change on a player, it needs to be pushed to a database.
"""

class Player:
    """
    This is a class describing a player. This is basically a player's sheet
    in a class.
    class attributes :
    String      player      the player's name
    Description description the info of a Description object
    Attributes  attributes  the info of an Attributes object
    Skills      skills      the info of a Skills object
    PlayersDB   DB          the database handler tied to this player
    """

    def __init__(self, name = None):
        """
        This function calls a function that will create a new character or will
        initialize a character from a database.
        """
        if not name == None:
            self.player = name
            self.initFromData(name)
        else:
            self.player      = None
            self.description = None
            self.attributes  = None
            self.skills      = None
            self.DB          = None


    def initFromData(self, name):
        """
        This task recreates a character from the data in a database.
        """
        self.DB = PlayersDB(name)
        self.DB.Connect()
        self.description = Description(self.DB.getDescription(), self)
        self.attributes = Attributes(self.DB.getAttributes(), self)
        self.skills = Skills(self.DB.getSkills(), self)
        self.DB.Disconnect()

    def updateChar(self):
        if self.player != None:
            self.DB.Connect()
            self.DB.updatePlayer(self)
            self.DB.Commit()
            self.DB.Disconnect()

    @staticmethod
    def newCharacter(description, attributes):
        """
        This function adds a new character to the database. Right now, this
        takes a table called description with the Player name as first element
        and the a list that can instanciate a description object in the rest.
        """
        # creating the player and inserting him in the database
        if PlayersDB.PlayerExists(description[0]):
            raise NameError('Player name ' + description[0] + ' already exists')
        if description[0] == '':
            raise NameError('Name is empty')
        PlayersDB.addPlayer(description[0])
        DB = PlayersDB(description[0])

        description = description[1:]
        description = Description(description)
        attributes = Attributes(attributes)
        DB.Connect()
        DB.addDescription(description)
        DB.addAttributes(attributes)
        DB.addSkills(skills)
        #print(self.DB.getDescription())
        # Abilites and HP
        # Skills
        # Feats
        # Spells
        # Inventory
        # Money
        # Initialization of all the other stuff
        # Committing the new character to the database
        DB.Commit()
        DB.Disconnect()

"""
Basically, static methods interract with the player list database, and non
static methods will interract with specific players so that each player object
can have a PlayersDB object incrusted in it.
"""
class PlayersDB:
    """
    This is a Player database manager. This is a collection of methods that
    manipulate the databases containing information about the players.
    """

    def __init__(self, name = None):
        """
        Creates a PlayersDB object. If a name is provided, this will be an
        object to connect to a specific player's database, otherwise, this will
        connect to the list of all players.
        """
        self.name = name
        self.conn = None
        self.cursor = None

    def Connect(self):
        """
        Connects to the database associated with this object. This has to be
        called explicitly.
        """
        try:
            if not self.name == None:
                self.conn = sqlite3.connect('Player/PlayersDB/' + self.name + '.db')
                self.cursor = self.conn.cursor()
            else:
                self.conn = sqlite3.connect('Player/PlayersDB/Players.db')
                self.cursor = self.conn.cursor()
                self.cursor.execute("SELECT * FROM sqlite_master WHERE type='table'")
                if not self.cursor.fetchone():
                    self.cursor.execute('''CREATE TABLE IF NOT EXISTS Players(
                        name text UNIQUE NOT NULL)''')
                    self.Commit()
        except Error as e:
            print(e)

    def Disconnect(self):
        """
        Disconnects from the database associated with this object. This has to
        be called explicitly.
        """
        self.cursor = None
        if self.conn:
            self.conn.close()
        self.conn = None

    def Commit(self):
        """
        Commits the changes applied to the database since the last commit. If
        this is not called before Disconnect, changes will be lost.
        """
        if self.conn:
            self.conn.commit()
        return

    def updatePlayer(self, player):
        """
        This updates this player in the databases.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        self.updateDescription(player.description)
        self.updateAttributes(player.attributes)
        self.updateSkills(player.skills)

    def addDescription(self, description):
        """
        This creates a description table for the Player. If a description table
        already exists, this does nothing.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if description.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Description(name text,
                race text, level integer, alignment text, deity text, size text,
                gender text, age integer, height real, weight real, hair text,
                eye text)''')
            self.cursor.execute('SELECT * FROM Description')
            if not self.cursor.fetchone():
                self.cursor.execute('''INSERT INTO Description VALUES(?,?,?,?,?,?,?,
                        ?,?,?,?,?)''', description.list)

    def getDescription(self):
        """
        This returns the description table contained in the Player's database.
        The elements are returned in a tuple where the data types already are
        Python data types.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        self.cursor.execute('''SELECT * FROM Description''')
        descr = self.cursor.fetchone()
        return descr

    def updateDescription(self, description):
        """
        This updates the description for the Player.
        This will create that table if there is none.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if description.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Description(name text,
                race text, level integer, alignment text, deity text, size text,
                gender text, age integer, height real, weight real, hair text,
                eye text)''')
            self.cursor.execute('SELECT * FROM Description')
            if not self.cursor.fetchone():
                self.cursor.execute('''INSERT INTO Description VALUES(?,?,?,?,?,?,?,
                        ?,?,?,?,?)''', description.list)
            else:
                #self.cursor.execute('SELECT * FROM Description')
                self.cursor.execute('''UPDATE Description SET name=?, race=?,
                level=?, alignment=?, deity=?, size=?, gender=?, age=?,
                height=?, weight=?, hair=?, eye=?''', description.list)

    def addAttributes(self, attributes):
        """
        This creates an attributes table for the Player. If an attributes table
        already exists, this does nothing.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if attributes.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Attributes(
                STR integer, CON integer, WIS integer, DEX integer, INT integer,
                CHA integer)''')
            self.cursor.execute('SELECT * FROM Attributes')
            if not self.cursor.fetchone():
                self.cursor.execute('''INSERT INTO Attributes VALUES(?,?,?,?,?,
                        ?)''', attributes.list)

    def getAttributes(self):
        """
        This returns the attributes table contained in the Player's database.
        The elements are returned in a tuple where the data types already are
        Python data types.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        self.cursor.execute('''SELECT * FROM Attributes''')
        attr = self.cursor.fetchone()
        return attr

    def updateAttributes(self, attributes):
        """
        This updates the attributes for player in the table.
        This will create the attributes table if it does not exist.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if attributes.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Attributes(
                STR integer, CON integer, WIS integer, DEX integer, INT integer,
                CHA integer)''')
            self.cursor.execute('SELECT * FROM Attributes')
            if not self.cursor.fetchone():
                self.cursor.execute('''INSERT INTO Attributes VALUES(?,?,?,?,?,
                        ?)''', attributes.list)
            else:
                self.cursor.execute('SELECT * FROM Attributes')
                self.cursor.execute('''UPDATE Attributes SET STR=?, CON=?, WIS=?,
                DEX=?, INT=?, CHA=?''', attributes.list)

    def addSkills(self, skills):
        """
        This creates a skills table for the Player. If an skills table
        already exists, this does nothing.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if skills.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Skills(
                Skill text, Rank text)''')
            self.cursor.execute('SELECT * FROM Skills')
            if not self.cursor.fetchone():
                for i in skills:
                    self.cursor.execute('''INSERT INTO skills VALUES(?,?)''', i)

    def getSkills(self):
        """
        This returns the skills table contained in the Player's database.
        The elements are returned in a list of tuples.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        self.cursor.execute('''SELECT * FROM Skills''')
        attr = self.cursor.fetchall()
        skills = {}
        for x in attr:
            skills[x[0]] = x[1]
        return skills

    def updateSkills(self, skills):
        """
        This updates the attributes for player in the table.
        This will create the attributes table if it does not exist.
        """
        if self.name == None:
            raise NameError('This method cannot be called on an unamed instance')
        # Checking if description contains all needed info.
        if skills.checkFull():
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS Skills(
                Skill text, Rank text)''')
            self.cursor.execute('SELECT * FROM Skills')
            if not self.cursor.fetchone():
                for i in skills:
                    self.cursor.execute('''INSERT INTO skills VALUES(?,?)''', i)
            else:
                for i in skills:
                    self.cursor.execute('''UPDATE Skills SET Rank=? WHERE
                            Skill=?''', (i[1], i[0]))

    def listTables(self):
        """
        Gets and returns a list of the tables stored in the player database.
        """
        self.cursor.execute("SELECT * FROM sqlite_master WHERE type='table'")
        names = []
        for table in self.cursor.fetchall():
            names.append(table[1])
        return names

    @staticmethod
    def PlayerExists(name):
        """
        Tests if name is the name of a player in the player list. name has to be
        a string. This will return True if the name already exists and False
        otherwise.
        """
        data = PlayersDB()
        data.Connect()
        data.cursor.execute("SELECT name FROM Players WHERE name=?", (name,))
        if data.cursor.fetchone():
            data.Disconnect()
            return True
        else:
            data.Disconnect()
            return False

    @staticmethod
    def addPlayer(name):
        """
        This method adds a player in the player list with the name name. name
        has to be a string.
        """
        if PlayersDB.PlayerExists(name):
            print('Could not add player because it already exists')
            return
        data = PlayersDB()
        data.Connect()
        data.cursor.execute('INSERT INTO Players VALUES(?)', (name,))
        data.Commit()
        data.Disconnect()

    @staticmethod
    def removePlayer(name):
        """
        This method adds a player in the player list with the name name. name
        has to be a string.
        """
        if not PlayersDB.PlayerExists(name):
            print("Player does not exists and can't be removed.")
            return
        os.remove('Player/PlayersDB/' + name + '.db')
        data = PlayersDB()
        data.Connect()
        data.cursor.execute('DELETE FROM Players WHERE name=?', (name,))
        data.Commit()
        data.Disconnect()

    @staticmethod
    def listPlayers():
        """
        This returns a Python list containing at each spot the name of one of
        the players.
        """
        data = PlayersDB()
        data.Connect()
        data.cursor.execute('SELECT * FROM Players')
        ret = data.cursor.fetchall()
        players = []
        for player in ret:
            players.append(player[0])
        return players

    @staticmethod
    def destroyDatabases():
        """
        This is a utility function that deletes all the files in the PlayersDB
        folder.
        """
        players = PlayersDB.listPlayers()
        for player in  players:
            os.remove('Player/PlayersDB/' + player + '.db')
        os.remove('Player/PlayersDB/Players.db')
