from data import *
"""
The different parts of a character sheet.
"""

"""
Maybe the set age function could create warnings in function of the age and the
race.
Maybe the class and race (and basically everything that is really important for
the character, shouldn't be in the description. If it stay in it, it could be
possible to make setters and getters in the Player object to interract easily
with those.
"""
class Description:
    """
    This class stores the description of a Player. Basically this is the top
    of the character sheet where some of the character information is stored
    such as the class and the race. This basically just stores a table, but
    since A LOT of information has to be maintained, it will be easier to
    manage setters and getters in separated classes.
    """

    def __init__(self, description, parent = None):
        """
        This will instanciate a description from a list containing, in order:
        string  Character's name
        string  Race
        int     Level
        string  Aligment
        string  Deity
        string  Size
        string  Gender
        int     Age
        float   Height (in meters)
        float   Weight (in kilograms)
        string  Hair
        string  Eye
        The parent object is the Player containing this description.
        """
        self.parent     = parent
        self.name       = None
        self.race       = None
        self.level      = None
        self.alignment  = None
        self.deity      = None
        self.size       = None
        self.gender     = None
        self.age        = None
        self.height     = None
        self.weight     = None
        self.hair       = None
        self.eye        = None
        self.list       = None

        self.setName(description[0])
        self.setRace(description[1])
        self.setLevel(description[2])
        self.setAlignment(description[3])
        self.setDeity(description[4])
        self.setSize(description[5])
        self.setGender(description[6])
        self.setAge(description[7])
        self.setHeight(description[8])
        self.setWeight(description[9])
        self.setHair(description[10])
        self.setEye(description[11])
        if self.checkFull():
            self.list = description

    def __str__(self):
        return str(self.list)

    def setName(self, name):
        """
        Sets the character name contained in this description.
        """
        if type(name) is str:
            self.name = name
            return True
        else:
            print("Cannot set character's name, needs to be a string.")
        return False

    def setRace(self, race):
        """
        Sets the race contained in this description.
        """
        if type(race) is str:
            if race in Constants.Races:
                self.race = race
                return True
            else:
                print("This race is not supported.")
        else:
            print("Cannot set character's race, needs to be a string.")
        return False

    def setLevel(self, level):
        """
        Sets the level contained in this description.
        """
        if type(level) is int:
            if 1 <= level and level <= 20:
                self.level = level
                return True
            else:
                print('Level needs to be in the range 1-20')
        else:
            print("Cannot set level, needs to be an integer.")
        return False

    def setAlignment(self, align):
        """
        Sets the alignment of this character.
        """
        if type(align) is str:
            if align in Constants.Alignments:
                self.alignment = align
                return True
            else:
                print("This is not a valid alignment.")
        else:
            print("Cannot set character's alignment, needs to be a string.")
        return False

    def setDeity(self, deity):
        """
        Sets the deity of this character.
        """
        if type(deity) is str:
            if deity in Constants.Deities:
                self.deity = deity
                return True
            else:
                print("This deity is not supported.")
        else:
            print("Cannot set character's deity, needs to be a string.")
        return False

    def setSize(self, size):
        """
        Sets the size of this player.
        """
        if type(size) is str:
            if size in Constants.Sizes:
                self.size = size
                return True
            else:
                print("This size is not supported.")
        else:
            print("Cannot set character's size, needs to be a string.")
        return False

    def setGender(self, gender):
        """
        Sets the gender of this character.
        """
        if type(gender) is str:
            self.gender = gender
            return True
        else:
            print("Cannot set character's gender, needs to be a string.")
        return False

    def setAge(self, age):
        """
        Sets the age contained in this description.
        """
        if type(age) is int:
            self.age = age
            return True
        else:
            print("Cannot set age, needs to be an integer.")
        return False

    def setHeight(self, height):
        """
        Sets the height contained in this description.
        """
        if type(height) is float:
            self.height = height
            return True
        else:
            print("Cannot set height, needs to be a float.")
        return False

    def setWeight(self, weight):
        """
        Sets the weight contained in this description.
        """
        if type(weight) is float:
            self.weight = weight
            return True
        else:
            print("Cannot set weight, needs to be a float.")
        return False

    def setHair(self, hair):
        """
        Sets the hair color of this character.
        """
        if type(hair) is str:
            self.hair = hair
            return True
        else:
            print("Cannot set character's hair color, needs to be a string.")
        return False

    def setEye(self, eye):
        """
        Sets the eye color of this character.
        """
        if type(eye) is str:
            self.eye = eye
            return True
        else:
            print("Cannot set character's eye color, needs to be a string.")
        return False

    def checkFull(self):
        """
        This checks if any field of this description is None. It returns True
        if all the fields contain a value other than none.
        """
        if(self.name == None or
                self.race == None or
                self.level == None or
                self.alignment == None or
                self.deity == None or
                self.size == None or
                self.gender == None or
                self.age == None or
                self.height == None or
                self.weight == None or
                self.hair == None or
                self.eye == None):
            return False
        return True

class Attributes:
    """
    This class stores the attributes of a character, as well as contains
    functions to compute bonus or malus incumbent to them.
    This class is indexable, starting from 0, using the order of the attributes
    in 'data.Constants.Attributes'.
    
    This class depends on the data package via the Attributes tuple in the
    Constants module.
    """

    def __init__(self, attributes, parent = None):
        """
        This will intanciate an Attributes object from a list containing the
        attributes specified in Constants.Attributes.
        parent is the Player object containing this Attributes.
        """
        self.parent = parent
        self.list = [None for i in Constants.Attributes]
        self.listMod = [0 for i in Constants.Attributes]
        for i in range(6):
            self[i] = attributes[i]

    def __str__(self):
        return str(self.list)

    def getMod(self, ind):
        """
        Get the modifier for the attribute ind.
        """
        return self.listMod[ind]

    def __getitem__(self, ind):
        """
        Get the attribute score for the attribute ind.
        """
        return self.list[ind]

    def __setitem__(self, ind, item):
        """
        Get the attribute score for the attribute ind.
        """
        self.list[ind] = item
        self.listMod[ind] = Attributes.evalMod(item)

    def checkFull(self):
        """
        This checks if any field filled by the constructor is None. It returns
        True if all the fields contain a value other than none.
        """
        for i in self.list:
            if i == None:
                return False
        return True

    @staticmethod
    def evalMod(value):
        """
        Evaluates the modifier that should be associated with an attribute value
        of value.
        """
        return (value - 10)//2

class Skills:
    """
    This is a structure to store the skills of a character.
    Since there are around 20 different skills, the skills themselves are stored
    in a dictionary. The keys in that dictionary are the skills names and the
    entries tied to those keys are the proficiency as well as the skill
    modifier.
    This sheet also contains perception because perception is basically a
    glorified skill.

    This class contains the most useful static method getProficiency that has
    use outside of this class.
    """

    def __init__(self, ranks, parent = None):
        """
        ranks is a list of tuples of the form
        ('skill name', 'skill proficiency')
        with skill proficiency being one of the letters U, T, E, M or L.
        
        parent is the Player this skill list is tied with. This is not usefull
        for now, but it will be needed for some game logic.
        """
        self.parent = parent
        self.skills = {}
        for x in ranks:
            self.skills[x] = ranks[x]
    
    def evalMod(self, skill):
        pass

    def checkFull(self):
        if len(self.skills) == len(SkillData.Skills):
            return True
        return False
            
    def __iter__(self):
        it = ((x, y) for x,y in self.skills.items())
        return it

    @staticmethod
    def getProficiency(player_level, skill_level):
        """
        Returns the proficiency of a player of a level 'player_level' at a skill
        of 'skill_level' where the skill level is
        0: untrained
        1: trained
        2: expert
        3: master
        4: legendary
        """
        if skill_level == 0:
            return player_level - 2
        elif skill_level == 1:
            return player_level
        elif skill_level == 2:
            return player_level + 1
        elif skill_level == 3:
            return player_level + 2
        elif skill_level == 4:
            return player_level + 3
