__all__ = ['Constants', 'SkillData']
"""
NOTE THAT WHAT IS IN THIS FOLDER IS NOT REALLY CURRENTLY A PACKAGE.

This folder contains all the data that is needed to execute the program.
This data goes from a list of classes to a database containing all the items
in the game (someday, when items will be implemented).

One day, there will a part of the interface to manage the database and the
data so that it is relatively easy to create new stuff, add it, modify
outdated info and remove old entries.
One day.

My current aim is for the program to basicaly be agnostic on what is in this
data folder so that the program is easy to extend and the rules easy to modify.
The program currently implements PF2e, but with this goal in mind I hope to
someday be able to implement stuff like PF1e, DD3.5e and DD 5e.
"""
