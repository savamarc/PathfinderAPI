"""
This contains tuples that list the limited values that certain variables can
take (such as alignments, classes, races). These lists will be used instead of
databases in constructors and the graphical interface because they will be
easier to use for an existence check.
"""
Attributes  = ('STR', 'CON', 'WIS', 'DEX', 'INT', 'CHA')
ProfRanks   = ('U', 'T', 'E', 'M', 'L')
ProfValues  = {'U':-2, 'T':0, 'E':1, 'M':2, 'L':3}
Alignments  = ('LG', 'NG', 'CG', 'LN', 'NN', 'CN', 'LE', 'NE', 'CE')
Races       = ('Dwarf', 'Elf', 'Gnome', 'Goblin', 'Halfling', 'Human')
Classes     = ('Alchemist', 'Barbarian', 'Bard', 'Cleric', 'Druid', 'Fighter',
        'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard')
Deities     = ('None',)
Sizes       = ('M',)
